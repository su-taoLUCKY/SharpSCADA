# SharpSCADA

#### Description
一款工业级轻量级组态软件
1.轻量级工控网关： 支持当前几种主要的工业协议如西门子的Profinet、AB的EtherNetIPs、施耐德的Modbus和OPC。采用类OPC接口网关。
2.数据采集、归档、预警及配置工具 支持实时数据采集、历史数据归档、变量触发预警，并使用TagConfig工具简单的配置实现。
3.人机界面（设计时和运行时）。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
